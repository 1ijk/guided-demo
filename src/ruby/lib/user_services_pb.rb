# Generated by the protocol buffer compiler.  DO NOT EDIT!
# Source: user.proto for package ''

require 'grpc'
require_relative 'user_pb'

module UserActivity
  class Service

    include GRPC::GenericService

    self.marshal_class_method = :encode
    self.unmarshal_class_method = :decode
    self.service_name = 'UserActivity'

    rpc :Acknowledge, UserInformation, UserAcknowledgement
  end

  Stub = Service.rpc_stub_class
end
