require './lib/user_services_pb'

module Demo
  class Friendly < UserActivity::Service
    def acknowledge(user_information, _extra)
      UserAcknowledgement.new(
        user: user_information,
        message: "Hello #{user_information.username}!"
      )
    end
  end
end