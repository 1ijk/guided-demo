require './lib/user_services_pb'

module Demo
  class Nasty < UserActivity::Service
    def acknowledge(user_information, _extra)
      UserAcknowledgement.new(
        user: user_information,
        message: "Who are you looking at, #{user_information.username}?"
      )
    end
  end
end