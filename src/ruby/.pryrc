require './lib/user_services_pb'

alice = UserInformation.new username: 'alice'
steve = UserInformation.new username: 'steve'

friendly = UserActivity::Stub.new('friendly.svc:50051', :this_channel_is_insecure)
nasty = UserActivity::Stub.new('nasty.svc:50051', :this_channel_is_insecure)