# Guided Demo

This is a guided demonstration on how to build an application using gRPC. Each section below represents one commit; you may follow along by reading and implementing yourself.

1. Design a protocol
2. Build language bindings
3. Use a `message`
4. Create a client
5. Implement a service
6. Make modifications
7. Containerize application (docker-compose)


## Creating a Protocol

You may think about gRPC as consisting of two aspects:

- a specification of data message constructs
- a specification of data communication patterns

It does not matter whether your role is a programmer, a product manager, a tester, or anything else in the organization. These two aspects of gRPC will help you reason about what the application does and how it does it.

The simplest protocol you can create looks like thus:

```
message UserInformation {
  string username = 1;
}
```

This specifies a template for data messages.

The simplest service you can create looks like thus:

```
service UserActivity {
  rpc Acknowledge(UserInformation) returns (UserInformation) {}
}
```

This specifies a template for data services.

At this stage, the key things to undestand is that the `message` describes what data gets communicated and the `service` describes the patterns for communicating with specified data.

Our example here merely defines a single message and a single pattern; the pattern here merely returns a message of the same type.

## Building Language Bindings

The protocol you have just created - in order to be used as part of running software - will need to be compiled into a target language binding. This simply means that the protocol itself is not runnable and that you will need to convert it to a runnable format.

Say you run code in the Ruby programming language. You will need to setup your project. Create a source directory for Ruby code and place a Gemfile.

```
.
├── README.md
├── protocols
│   └── user.proto
└── src
    └── ruby
        └── Gemfile
```

Specify in the Gemfile that you'll be using gRPC:

```
source 'https://rubygems.org'

gem 'grpc'
gem 'grpc-tools'
```

Perform a `bundle install` of those gems, then generate the bindings:

```
cd src/ruby
bundle install
mkdir -p lib
grpc_tools_ruby_protoc -I../../protocols --ruby_out=lib ../../protocols/*.proto
cd -
```

You should now have the following created in your project:

```
.
├── README.md
├── protocols
│   └── user.proto
└── src
    └── ruby
        ├── Gemfile
        ├── Gemfile.lock
        └── lib
            └── user_pb.rb
```

That last file, `user_pb.rb`, was *generated* by the gRPC toolchain from the protocol buffer file. You will use it to implement your gRPC service and call the service from your clients.

## Using a `message`

Before jumping into implementing services and clients, take a moment to get a feel for the generated code. Since you're using Ruby, you can use the `pry` gem to play with the generated code on the command line. Add it to your Gemfile:

```
source 'https://rubygems.org'

gem 'grpc'
gem 'grpc-tools'
gem 'pry-byebug'
```

install, and start:

```
cd src/ruby
bundle install
bundle exec pry
```

You can then load the generated code and interact with it:

```
pry(main)> require './lib/user_pb'
=> true
pry(main)> alice = UserInformation.new username: 'alice'
=> <UserInformation: username: "alice">
pry(main)> alice.to_h
=> {:username=>"alice"}
pry(main)> alice.to_json
=> "{\"username\":\"alice\"}"
pry(main)> steve = UserInformation.new username: 'alice'
=> <UserInformation: username: "alice">
pry(main)> alice == steve
=> true
pry(main)> steve.username = 'steve'
=> "steve"
pry(main)> alice == steve
=> false
```

Playing around like this, you see that the generated code has intitialiation parameters, getter and setter methods, and basic methods to compare and transmute the object to other data types, such as a Hash or JSON formatted string.

This code generation is very useful for programming purposes as it takes care of much boiler plate for you. Even more importantly, the generated code contains a function `to_proto` that packs the data into a binary format. It's this binary format that will be sent over HTTP/2 between gRPC services and clients.

## Creating a client

Above, when you generated language bindings, I did not give the instruction for the gRPC service portion of the code. You may run that generation now with:

```
grpc_tools_ruby_protoc -I../../protocols --ruby_out=lib --grpc_out=lib ../../protocols/*.proto
```

and notice that one new file gets created:

```
.
├── README.md
├── protocols
│   └── user.proto
└── src
    └── ruby
        ├── Gemfile
        ├── Gemfile.lock
        └── lib
            ├── user_pb.rb
            └── user_services_pb.rb
```

There is a slight bug in the generation here, however. You can fix it by opening up `user_services_pb.rb` and changing `require 'user_pb'` to `require_relative 'user_pb'`.

Reload `pry` and see what's now available:

```
pry(main)> require './lib/user_services_pb'
=> true
pry(main)> alice = UserInformation.new username: 'alice'
=> <UserInformation: username: "alice">
pry(main)> activity = UserActivity::Stub.new('localhost:50051', :this_channel_is_insecure)
=> #<UserActivity::Stub:0x00007fb23f9f4860
 @ch=#<GRPC::Core::Channel:0x00007fb23f9f46f8>,
 @host="localhost:50051",
 @interceptors=#<GRPC::InterceptorRegistry:0x00007fb23f9f46a8 @interceptors=[]>,
 @propagate_mask=nil,
 @timeout=1970-01-01 08:59:59 +0900>
pry(main)> activity.acknowledge alice
GRPC::Unavailable: 14:failed to connect to all addresses
from /Users/jared/.rbenv/versions/2.5.5/lib/ruby/gems/2.5.0/gems/grpc-1.23.0-universal-darwin/src/ruby/lib/grpc/generic/active_call.rb:31:in `check_status`
```

Let's break this down:

1. You load the generated code
2. You create a gRPC message
3. You create a gRPC client
4. You (attempt to) send a gRPC message via the client

You've done (1) and (2) previously, (3) and (4) are new. By way of explanation, creating a gRPC client is the same as creating a gRPC service "stub". The sole purpose of this client is to package the gRPC messages in the binary protocol buffer format and send them to the indicated endpoint. Your attempt to send the message fails because we do not have a service endpoint implemented or running.

This is OK. You've now seen that you have everything on your client side required to create and send messages.

## Configure our pry repl

To make things easier going forward, we can configure our pry repl using a .pryrc file:

```
require './lib/user_services_pb'

alice = UserInformation.new username: 'alice'
steve = UserInformation.new username: 'steve'

activity = UserActivity::Stub.new('localhost:50051', :this_channel_is_insecure)
```

Now every time you run `pry`, this code will execute, making its results immediately available.

## Implementing a service

You can finally write some interesting business logic at this point. It may seem like a lot of boiler plate up until now, but it will have saved you quite a bit of time in the long-run as you will not need to worry about client creation or proxy configuration.

You can create a class in `lib/demo/friendly.rb` and give it methods corresponding to your gRPC service declaration:

```ruby
require './lib/user_services_pb'

module Demo
  class Friendly < UserActivity::Service
    def acknowledge(user_information, _extra)
      UserInformation.new(username: user_information.username)
    end
  end
end
```

This matches the gRPC signature declaration of the protocol and will be runnable as an endpoint. A simple script will suffice to run the endpoint:

```
#!/usr/bin/env ruby

require 'grpc'
require './lib/demo/friendly'

server = GRPC::RpcServer.new
server.add_http2_port('localhost:50051', :this_port_is_insecure)
server.handle(Demo::Friendly)
server.run_till_terminated
```

Open two terminal windows. In the first, run:

```
chmod +x bin/friendly
bundle exec bin/fiendly
```

In the second, run in `pry`:

```
pry(main)> friendly.acknowledge alice
=> <UserInformation: username: "alice">
pry(main)> friendly.acknowledge steve 
=> <UserInformation: username: "steve">
```

It works! Your client - running within `pry` - sends messages to the service, and the service responds with a new, identical message.

# Making modifications

The simple service you've created isn't all that interesting. Let's say we now would like to send a message with the acknowledgement. We'll create a new protocol message:

```
message UserAcknowledgement {
  UserInformation user = 1;
  string message = 2;
}
```

that will send back the original information along with a message string. Then we'll ammend the service definition:

```
service UserActivity {
  rpc Acknowledge(UserInformation) returns (UserAcknowledgement) {}
}
```

Our implementation, of course, must change:

```ruby
require './lib/user_services_pb'

module Demo
  class Friendly < UserActivity::Service
    def acknowledge(user_information, _extra)
      UserAcknowledgement.new(
        user: user_information.username,
        message: "Hello #{user_information.username}!"
      )
    end
  end
end
```

We need to re-generate the bindings, too:

```
grpc_tools_ruby_protoc -I../../protocols --ruby_out=lib --grpc_out=lib ../../protocols/*.proto
```

Fix the `require` bug by opening up `user_services_pb.rb` and changing `require 'user_pb'` to `require_relative 'user_pb'`.

Then re-run the script and retest in the repl:

```
pry(main)> friendly.acknowledge steve
=> <UserAcknowledgement: user: <UserInformation: username: "steve">, message: "Hello steve!">
```

Good; we've a slightly more interesting service.

## Use Docker

In general, we don't want to run such services by opening individual terminal windows. The advantages of gRPC become more relevant if we package services in a containerization framework like Docker or Kubernetes, allowing us to isolate a consistent runtime.

Create a Dockerfile for our Ruby code:

```
FROM ruby:2.5

RUN gem install bundler

COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock

RUN bundle install

COPY .pryrc .pryrc
COPY bin bin
COPY lib lib

ENTRYPOINT [ "bundle", "exec" ]
```

and for basic testing, create a `docker-compose.yaml` file to launch the client and server environments:

```
version: '3'
services:
  ruby_client:
    image: 'jprime/guided-demo:ruby'
    build:
      context: src/ruby
    command: pry
  ruby_friendly_server:
    image: 'jprime/guided-demo:ruby'
    build:
      context: src/ruby
    command: bin/friendly
```

We can run the build and launch the services with one command: `docker-compose up -d`

Now, connect to the test container:

```
docker attach guided-demo_ruby_client
pry(main)> friendly.acknowledge alice
=> <UserAcknowledgement: user: <UserInformation: username: "steve">, message: "Hello alice!">
pry(main)> friendly.acknowledge steve
=> <UserAcknowledgement: user: <UserInformation: username: "steve">, message: "Hello steve!">
```

Things get more interesting when we add different service implementations. For instance, create a "nasty" implementation:

```ruby
require './lib/user_services_pb'

module Demo
  class Nasty < UserActivity::Service
    def acknowledge(user_information, _extra)
      UserAcknowledgement.new(
        user: user_information,
        message: "Who are you looking at, #{user_information.username}?"
      )
    end
  end
end
```

and a script to run it:

```
#!/usr/bin/env ruby

require 'grpc'
require './lib/demo/nasty'

server = GRPC::RpcServer.new
server.add_http2_port('0.0.0.0:50051', :this_port_is_insecure)
server.handle(Demo::Nasty)
server.run_till_terminated
```

Basically everything is the same execpt the class defining the business logic. Add the nasty service to the docker-compose:

```
version: '3'
services:
  ruby_client:
    image: 'jprime/guided-demo:ruby'
    build:
      context: src/ruby
    command: pry
    links:
      - ruby_friendly_server:friendly.svc
      - ruby_nasty_server:nasty.svc
  ruby_friendly_server:
    image: 'jprime/guided-demo:ruby'
    build:
      context: src/ruby
    command: bin/friendly
  ruby_nasty_server:
    image: 'jprime/guided-demo:ruby'
    build:
      context: src/ruby
    command: bin/nasty
```

and a quick client to the `.pryrc`

```
nasty = UserActivity::Stub.new('nasty.svc:50051', :this_channel_is_insecure)
```

And you can test as before.